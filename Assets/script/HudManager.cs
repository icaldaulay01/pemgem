using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HudManager : MonoBehaviour
{
    public Image currentEnergy;
    public Text time;

    private GameObject player;
    public Player playerInstance;

    private float health;
    private float maxHealth = 100f;
    public Image currentHealth;

    private int score;
    public Text scoreText;

    private float energy = 100;
    private float maxEnergy = 100;
    private float kecepatan;
    private float kecepatanLari;
    private float input_x;
    private float input_y;

    public static bool GameIsPaused = false;
    public GameObject pauseMenuUI;


    void Start()
    {
        player = GameObject.Find("Player");
        kecepatanLari = 1.5f;
    }

    // Update is called once per frame
    void Update()
    {
        kecepatan = player.GetComponent<Player_Movement>().isrun;
        input_x = player.GetComponent<Player_Movement>().x;
        input_y = player.GetComponent<Player_Movement>().y;
        health = player.GetComponent<HealthSystem>().playerHealth;
        score = player.GetComponent<Player>().score;

        showPauseMenu();
        updateHealth();
        EnergyDrain();
        UpdateEnergy();
        UpdateTime();
        UpdateScore();
    }

    private void UpdateScore()
    {
        scoreText.text = "Your Score: " + score.ToString();
    }

    private void EnergyDrain()
    {
        if (kecepatan >= kecepatanLari)
        {
            if (input_x != 0 || input_y != 0)
            {
                if (energy > 0)
                {
                    energy -= 10 * Time.deltaTime;
                }

            }
        }
        else
        {
            energy += 15 * Time.deltaTime;
        }


    }
    private void UpdateEnergy()
    {
        float ratio = energy / maxEnergy;
        currentEnergy.rectTransform.localScale = new Vector3(ratio, 1, 1);
    }

    private void UpdateTime()
    {
        int hours = EnviroSky.instance.GameTime.Hours;
        int minutes = EnviroSky.instance.GameTime.Minutes;
        string gameHours;
        string gameMinutes;

        if (hours >= 0 && hours < 10)
        {
            gameHours = "0" + hours.ToString();
        }
        else
        {
            gameHours = hours.ToString();
        }
        if (minutes >= 0 && minutes < 10)
        {
            gameMinutes = "0" + minutes.ToString();
        }
        else
        {
            gameMinutes = minutes.ToString();
        }

        time.text = gameHours + " : " + gameMinutes;
    }

    private void showPauseMenu()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                ResumeGame();
            }
            else
            {
                PauseGame();
            }
        }
    }

    public void ResumeGame()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    void PauseGame()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
        Cursor.lockState = CursorLockMode.Confined;
    }

    private void updateHealth()
    {
        float ratio = health / maxHealth;
        currentHealth.rectTransform.localScale = new Vector3(ratio, 1, 1);
    }

    public void SaveGame()
    {
        SaveSystem.SavePlayer(playerInstance);
    }



}