using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthSystem : MonoBehaviour
{
    public float playerHealth = 100f;
    public string info;

    // Start is called before the first frame update
    void Start()
    {
        playerHealth = 100f;
    }

    // Update is called once per frame
    void Update()
    {
        if (playerHealth > 0)
        {
            Debug.Log("Player hidup");
        }
        else
        {
            Debug.Log("Player mati");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Obstacle")
        {
            playerHealth -= 20f;
            info = "You can't go to isekai using mushroom u d*mb as*";
        }

        if (other.tag == "Enemy")
        {
            playerHealth -= 10f;
            info = "You know u'r MC right? Why u died?";
        }

        if (other.tag == "Fire")
        {
            playerHealth -= 10f;
            info = "I know u'r hungry, but don't burn yourself";
        }
    }
}
